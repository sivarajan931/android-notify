package com.example.mnotify;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.os.Build;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        createNotificationChannel();
        EditText et1 = findViewById(R.id.et1);
        findViewById(R.id.button).setOnClickListener(v -> {
            // Builder helps in create notification
            NotificationCompat.Builder builder = new NotificationCompat.Builder(this, "siva")
                    //NotificationCompat.Builder(context,ChannelId)
                    .setSmallIcon(R.drawable.ic_launcher_background)
                    .setContentTitle("Notify") //Title
                    .setContentText(et1.getText().toString()) //Sub text
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT);

            NotificationManagerCompat nm = NotificationManagerCompat.from(this);
            // Get Notification instance of the particular app
            nm.notify(1, builder.build());
            // notify(id,notification)
            // builder.build  will generate notification object
        });

    }

    private void createNotificationChannel() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            // A channel must be initialized for pushing notification in it after Android O
            // This is done only at Initial Stage
            NotificationChannel channel = new NotificationChannel("siva", "Channel", NotificationManager.IMPORTANCE_DEFAULT);
            // NotificationChannel(id,channel_name,Importance);
            // this id is used in notification builder
            channel.setDescription("App Notify Channel"); // set channel Description
            NotificationManager notificationManager = getSystemService(NotificationManager.class);

            notificationManager.createNotificationChannel(channel);
            // get the system notification APi and create a notification channel for the app.
        }
    }
}